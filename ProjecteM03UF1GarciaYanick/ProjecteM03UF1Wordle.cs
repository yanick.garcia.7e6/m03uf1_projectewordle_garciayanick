﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ProjecteM03GarciaYanick
{
    public class ProjecteM03Wordle
    {
        /*CREADOR: Yanick Garcia
         *FECHA: 22/2/2023-28/2/2023*/

        /// <summary>
        /// Nombre: Wordle
        /// 
        /// Descripción: Con todo lo aprendido sobre ficheros y modulación se ha refinado
        /// el anterior proyecto que hicimos. Una versión algo más simple del popular juego
        /// de adivinar una palabra conocido como wordle. Esta versión dispone de diversas
        /// nuevas caracteristicas que la hacen más completa y sofisticada respecto a la anterior.
        /// 
        /// Cambios:
        /// -Se ha incrementado la lista de palabras de 20 a 100.
        /// -Se ha añadido un menú desde el cual el jugador podrá escoger entre distintas opciones.
        /// -Se han implementado ficheros para disponer de soporte en dos idiomas.
        /// -Se ha añadido la posibilidad de hacer partidas consecutivas
        /// -Ahora los jugadores pueden guardar su puntuación en un documento dónde se especifican su nombre
        /// número de intentos e idioma en el que han jugado.
        /// -Colores han sido añadidos a los textos para hacerlo más visual.
        /// -Se ha añadido una opción que te impedirá jugar al juego si no escoges idioma correctamente.
        /// </summary>
        static void Main()
        {
            var inici = new ProjecteM03Wordle();
            inici.Inici();
        }

        public void Inici()
        {
            ///En inicio se crea un path que se aplicará para acceder a los ficheros en función del idioma.
            ///Después se comprobará si existe el fichero de puntuaciones y en caso de no ser así se creará uno.
            ///
            string path = @"E:\Garcia Yanick\M03Projecte_YanickGarcia\";
            string score = path + "allScores.txt";
            if (!File.Exists(score))
            {
                var scoreDoc = File.Create(score);
                scoreDoc.Close();
                Console.WriteLine("Se ha creado un fichero de puntuaciones!");
            }
            string messages = "";
            string directori = " ";
            int noJuegas = 0;
            string instr = "";
            string lang;
            string rndFile = "";
            bool acces = false;
            //Selección de idiomas
            do
            {
                ///Selección de idiomas, el usuario con esto escoje en que idioma jugar, se rige por un bool
                ///que se activará si elige un idioma permitido.
                Console.WriteLine("Elige idioma:\nEng.\nEs.\n");
                lang = Console.ReadLine();
                acces = idiomes(lang);
                if (acces == true)
                {
                    directori = path + @$"{lang}\Menu.txt";
                    rndFile = path + @$"{lang}\Rnd.txt";
                    instr = path + @$"{lang}\Instr.txt";
                    messages = path + @$"{lang}\Msg.txt";
                    break;
                }
                //Opción extra, si el usuario no elige bien el idioma 10 veces se le impide jugar.
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR.\n");
                    Console.ResetColor();
                    noJuegas++;
                    if (noJuegas == 10)
                    {
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("\nTÚ NO JUEGAS POR NO SABER ESCRIBIR.");
                        string[] learn = File.ReadAllLines(path + "noJuegas.txt");
                        for (int i = 0; i < learn.Length; i++)
                        {
                            Console.WriteLine(learn[i]);
                        }
                        Console.ResetColor();
                        Environment.Exit(0);
                    }
                }
            } while (acces != true);
            int opcio;
            do
            {
                Console.Clear();
                Menu(directori, rndFile, instr);
                opcio = InsertarOpcion(directori, rndFile, instr, score, lang, messages);
            } while (opcio != 0);
        }
        ///Creación de un menú que se imprimirá en pantalla.
        public void Menu(string directori, string rndFile, string instr)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(@"                                         __  __           
                                        |  \|  \          
 __   __   __   ______    ______    ____| $$| $$  ______  
|  \ |  \ |  \ /      \  /      \  /      $$| $$ /      \ 
| $$ | $$ | $$|  $$$$$$\|  $$$$$$\|  $$$$$$$| $$|  $$$$$$\
| $$ | $$ | $$| $$  | $$| $$   \$$| $$  | $$| $$| $$    $$
| $$_/ $$_/ $$| $$__/ $$| $$      | $$__| $$| $$| $$$$$$$$
 \$$   $$   $$ \$$    $$| $$       \$$    $$| $$ \$$     \
  \$$$$$\$$$$   \$$$$$$  \$$        \$$$$$$$ \$$  \$$$$$$$
                                                         ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            string[] menuText = File.ReadAllLines(directori);
            for (int i = 0; i < menuText.Length; i++)
            {
                if (menuText[i].Contains("1") || menuText[i].Contains("2") || menuText[i].Contains("3") || menuText[i].Contains("4") || menuText[i].Contains("5"))
                    Console.WriteLine(menuText[i] + "\n");
            }
            Console.ResetColor();
        }

        ///Selección de opciones, printa desde un archivo el menú del juego.
        public int InsertarOpcion(string directori, string rndFile, string instr, string score, string lang, string messages)
        {
            int opcio;

            do
            {

            } while (!int.TryParse(Console.ReadLine(), out opcio));

            switch (opcio)
            {
                case 0:
                    break;
                case 1:
                    Console.Clear();
                    Jugar(directori, rndFile, score, lang, messages);
                    Console.ReadLine();
                    break;
                case 2:
                    Console.Clear();
                    Instrucciones(instr);
                    Console.ReadLine();
                    Menu(directori, rndFile, instr);
                    break;
                case 3:
                    Console.Clear();
                    MostrarPuntuaciones(score);
                    Console.ReadLine();
                    break;
                case 4:
                    Console.Clear();
                    Inici();
                    break;
                case 5:
                    Console.Clear();
                    Salir(messages);
                    Console.ReadLine();
                    break;
                default:
                    break;

            }
            return opcio;
        }

        ///El juego en sí
        public void Jugar(string directori, string rndFile, string score, string lang, string messages)
        {
            string[] whichMsg = File.ReadAllLines(messages); //Escribe el mensaje inicial
            string playAgain = "";
            string name = "";
            int counter = 0;
            string hoyAdivinas = "";
            bool apt;
            bool isNum;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine("\t\t\t\t" + whichMsg[0]);
                Console.ResetColor();
                counter = 0;
                hoyAdivinas = getRandom(hoyAdivinas, rndFile);
                //Console.WriteLine(hoyAdivinas); //Descomentar en caso de querer ver la palabra

                ///Las palabras pasarán por todas estas iteraciones y las cuales comprobarán el tamaño y el contenido de las frases.
                ///
                for (int i = 0; i < 6; i++)
                {
                    counter++;
                    Console.Write("- ");
                    string escrito = Console.ReadLine();
                    apt = escritoSize(escrito);
                    isNum = contaiNum(escrito);
                    //Condición de rechazo
                    if (apt == false || isNum == false)
                    {
                        Console.WriteLine(whichMsg[1]);
                        i--;
                        counter--;
                    }
                    else if (apt == true && escrito != hoyAdivinas)
                    {
                        printNum(hoyAdivinas, escrito);
                        if (counter == 6)
                        {
                            Console.ResetColor();
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("\n\n" + whichMsg[9]);
                        }
                        Console.Write("\n");
                        Console.ResetColor();
                    }

                    //Condición de victoria
                    else if (apt == true && escrito == hoyAdivinas)
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.WriteLine(escrito);
                        Console.ResetColor();
                        Console.WriteLine(whichMsg[2]);
                        Console.WriteLine(whichMsg[3]);
                        name = Console.ReadLine();
                        StreamWriter sw = File.AppendText(score);
                        sw.WriteLine("Player: " + name + "\nTries: " + counter + "\nLang: " + lang + "\n\n");
                        sw.Close();
                        break;
                    }
                }
                Console.WriteLine(whichMsg[4] + " " + hoyAdivinas);
                Console.WriteLine($"{whichMsg[5]}\n {whichMsg[6]} \n {whichMsg[7]}");
                playAgain = Console.ReadLine().ToLower();
            } while (playAgain == "si" || playAgain == "yes");
        }

        ///Booleano que se asegura de que la palabra sea de 5 letras.
        public bool escritoSize(string escrito)
        {
            int size = escrito.Length;
            if (size == 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ///Escoge de manera aleatoria una de las palabras contenidas en el archivo correspondiente
        public static string getRandom(string hoyAdivinas, string rndFile)
        {
            Random palabras = new Random();
            string palabras5Let = File.ReadAllText(rndFile);
            string[] palabras5LetString = palabras5Let.Split(" ");
            int numPalabra = palabras.Next(palabras5LetString.Length);
            hoyAdivinas = palabras5LetString[numPalabra];
            return hoyAdivinas;
        }
        ///Se encarga de escribir y cambiar el color de fond de cada caracter según si es o no correcta o si aparece
        public void printNum(string hoyAdivinas, string escrito)
        {

            for (int let = 0; let < 5; let++)
            {
                if (escrito[let] == hoyAdivinas[let])
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.Write(escrito[let]);
                }
                else
                {
                    if (hoyAdivinas.Contains(escrito[let]))
                    {
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.Write(escrito[let]);
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                        Console.Write(escrito[let]);
                    }
                }
            }
        }
        ///Printa el documento de instrucciones
        public void Instrucciones(string instr)
        {
            Console.Clear();
            string[] instText = File.ReadAllLines(instr);
            for (int i = 0; i < instText.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine(instText[i] + "\n");
                Console.ResetColor();
            }
        }

        ///Abre el archivo en el que se guardan las puntuaciones
        public void MostrarPuntuaciones(string score)
        {
            if (File.Exists(score))
            {
                StreamReader sr = File.OpenText(score);
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("\n" + sr.ReadToEnd());
                Console.ResetColor();
                sr.Close();
            }
        }
        ///Sale del programa
        public void Salir(string messages)
        {
            string[] lastMsg = File.ReadAllLines(messages);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(lastMsg[8]);
            Console.ResetColor();
            Environment.Exit(0);
        }
        ///Comprueba si la entrada de idioma es o no valida.
        public bool idiomes(string lang)
        {
            if (lang == "es" || lang == "eng")
            {
                return true;

            }
            else
            {
                return false;
            }
        }
        ///Comprueba que la palabra del usuario no contenga números
        public bool contaiNum(string escrito)
        {
            bool save = true;
            if (escrito.Contains("0") || escrito.Contains("1") || escrito.Contains("2") || escrito.Contains("3") || escrito.Contains("4") || escrito.Contains("5") || escrito.Contains("6") || escrito.Contains("7") || escrito.Contains("8") || escrito.Contains("9"))
            {
                save = false;
            }
            return save;
        }
    }
}

